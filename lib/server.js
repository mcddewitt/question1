'use strict';

const http = require('http');

module.exports =  class Server {
  constructor(address, port, requestHandler) {
    this.address = address;
    this.port = port;
    this.requestHandler = requestHandler;
    this.server = http.createServer((req, res) => {
      this.requestHandler.handleRequest(req,res);
    });
  }

  connect() {
    this.server.listen(this.port, this.hostname);  	
  }
};

