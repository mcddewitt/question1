'use strict';

const RequestHandler = require('./handler');
const Server = require('./server');

module.exports = class NodeServer {
  constructor(host, port) {
    this.host = host;
    this.port = port;
    this.appContext = {};
    this.requestHandler = new RequestHandler(this.appContext);
    this.server = new Server(this.host, this.port, this.requestHandler);
  }

  get(path, next) {
    this.requestHandler.addRoute(path, 'GET', next);
  }

  post(path, next) {
    this.requestHandler.addRoute(path, 'POST', next);
  }

  put(path, next) {
    this.requestHandler.addRoute(path, 'PUT', next);
  }
  delete(path, next) {
    this.requestHandler.addRoute(path, 'DELETE', next);
  }

  error(code, next) {
    this.requestHandler.addErrorHandler(code, next);
  }

	//Set a value that will be attached to request.app object
  set(key, value) {
    this.appContext[key] = value;
  }

  run() {
    this.server.connect();
    console.log('Server now running at %s:%s', this.host, this.port);
  }

};

