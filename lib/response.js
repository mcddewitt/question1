'use strict';

module.exports = class Response {
  constructor(res) {
    // Same as Request class.  This needs some work.
    this.res = res;
    this.header = {};
    this.body = null;
    this.status = 200;
  }

  writeHeader(header) {
    this.res.writeHead(this.status,
      header
    );
  }

  write() {
    this.res.writeHead(this.status, this.header);
    this.res.write(this.body);
    this.res.end();
  }

  json(jsonVal) {
    this.body = JSON.stringify(jsonVal);
    this.header['Content-Length'] = this.body.length;
    this.header['Content-Type'] = 'application/json';
    this.write(jsonVal);

  }

  errorJson(status, code, message, description) {
    this.status = status;
    this.json({
      result: 'error',
      code: code,
      message: message,
      description: description
    });
  }

};