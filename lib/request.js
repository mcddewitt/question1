'use strict';

const url = require('url');
const querystring = require('querystring');

module.exports = class Request {
  constructor(req, ctx) {
    // This isn't nice.  Had some problems referencing 
    // node request.  Definitely fixable.
    this.req = req;
    this.method = req.method;
    this.headers = req.headers;
    this.body = {};
    this.app = ctx;
    this.query = querystring.parse(url.parse(req.url).query);
    this.params = {};
  }

  parseBody(next) {
    this.req.on('data', (data) => {
      this.body = JSON.parse(data.toString('utf8'));
    }).on('end', () => {
      next();
    });
  }
};