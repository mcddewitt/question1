'use strict';

var crypto = require('crypto');
var NodeServer = require("../lib/app");
var utils = require("./utils");
var mock = require("./mock");
var authRoutes = require('./routes/auth');

var userData = [];

function loadFakeData(next) {
  for( let i = 0; i < mock.users.length; i++) {
    let user = mock.users[i];
    utils.loadFakeUserData(user.username, user.password, (u) => {
      userData.push(u); 
      if(i === mock.users.length - 1) {
        next();
      }
   });
  };
}

function setupApp() {
  loadFakeData( () => {
    const app = new NodeServer("localhost", 5000);

    //Create routes. 
    app.post("/api/v1/login", authRoutes.login);
    app.post("/api/v1/logout", authRoutes.logout);
    
    //Another way to load a route.
    //his route is used for returns of invalid endpoints.
    app.error(404, (req, res) => {
      res.errorJson(404, 2222, "Invalid Endpoint", "Endpoint Not Found" );
    });

    //Using this to just easily access my user objects.
    app.set("users", userData);

    app.run();  
  });
}

//Create App
setupApp();

