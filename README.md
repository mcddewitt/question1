# Question 1 - David DeWitt
This is the examples used to login and logout.  Due to time some of the token auth is not exactly correct but shows the process of logging in and out.


To start:
run npm start from top directory                                     
Server defaults to localhost:5000  

Login:

POST /api/v1/login HTTP/1.1

Value pass:
{
username: username, password: password
}

The returned token can be simply placed in the Authorization header

Authorization: <token>


Logout:
POST /api/v1/logout HTTP/1.1
